#ifndef LAMBDA_TYPES_H
#define LAMBDA_TYPES_H

#include <wayland-server-core.h>
#include <xkbcommon/xkbcommon.h>

#include <wlr/backend.h>
#include <wlr/render/allocator.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_pointer.h>
#include <wlr/types/wlr_primary_selection_v1.h>
#include <wlr/types/wlr_scene.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_viewporter.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>

/* For brevity's sake, struct members are annotated where they are used. */

enum { CurNormal, CurMove, CurResize }; /* cursor */
enum { XDGShell, LayerShell, X11Managed, X11Unmanaged }; /* client types */
enum { LyrBg, LyrBottom, LyrTop, LyrOverlay, LyrTile, LyrFloat, LyrNoFocus, NUM_LAYERS }; /* scene layers */
#ifdef XWAYLAND
enum { NetWMWindowTypeDialog, NetWMWindowTypeSplash, NetWMWindowTypeToolbar,
	NetWMWindowTypeUtility, NetLast }; /* EWMH atoms */
#endif

typedef union {
  int i;
  unsigned int ui;
  float f;
  const void *v;
} Arg;

typedef struct {
  unsigned int mod;
  unsigned int button;
  void (*func)(const Arg *);
  const Arg arg;
} Button;

typedef struct Monitor Monitor;
typedef struct {
  /* Must keep these three elements in this order */
  unsigned int type;   /* XDGShell or X11* */
  struct wlr_box geom; /* layout-relative, includes border */
  Monitor *mon;
  struct wlr_scene_node *scene;
  struct wlr_scene_rect *border[4]; /* top, bottom, left, right */
  struct wlr_scene_node *scene_surface;
  struct wlr_scene_rect *fullscreen_bg; /* See setfullscreen() for info */
  struct wl_list link;
  struct wl_list flink;
  union {
    struct wlr_xdg_surface *xdg;
    struct wlr_xwayland_surface *xwayland;
  } surface;
  struct wl_listener commit;
  struct wl_listener map;
  struct wl_listener unmap;
  struct wl_listener destroy;
  struct wl_listener set_title;
  struct wl_listener fullscreen;
  struct wlr_box prev; /* layout-relative, includes border */
#ifdef XWAYLAND
  struct wl_listener activate;
  struct wl_listener configure;
  struct wl_listener set_hints;
#endif
  int bw;
  unsigned int tags;
  int isfloating, isurgent;
  uint32_t resize; /* configure serial of a pending resize */
  int isfullscreen;
} Client;

typedef struct {
  uint32_t singular_anchor;
  uint32_t anchor_triplet;
  int *positive_axis;
  int *negative_axis;
  int margin;
} Edge;

typedef struct {
  uint32_t mod;
  xkb_keysym_t keysym;
  void (*func)(const Arg *);
  const Arg arg;
} Key;

typedef struct {
  struct wl_list link;
  struct wlr_input_device *device;

  struct wl_listener modifiers;
  struct wl_listener key;
  struct wl_listener destroy;
} Keyboard;

typedef struct {
  /* Must keep these three elements in this order */
  unsigned int type; /* LayerShell */
  struct wlr_box geom;
  Monitor *mon;
  struct wlr_scene_node *scene;
  struct wl_list link;
  int mapped;
  struct wlr_layer_surface_v1 *layer_surface;

  struct wl_listener destroy;
  struct wl_listener map;
  struct wl_listener unmap;
  struct wl_listener surface_commit;
} LayerSurface;

struct Monitor {
  struct wl_list link;
  struct wlr_output *wlr_output;
  struct wlr_scene_output *scene_output;
  struct wl_listener frame;
  struct wl_listener destroy;
  struct wlr_box m;         /* monitor area, layout-relative */
  struct wlr_box w;         /* window area, layout-relative */
  struct wl_list layers[4]; /* LayerSurface::link */
  // const Layout *lt[2];
  unsigned int seltags;
  unsigned int sellt;
  unsigned int tagset[2];
  double mfact;
  int nmaster;
  int un_map; /* If a map/unmap happened on this monitor, then this should be
                 true */
};

#endif // !LAMBDA_TYPES_H
