#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>
#define UNUSED(SYMBOL) (void)(SYMBOL);

void die(const char *fmt, ...);

void *ecalloc(size_t nmemb, size_t size);

#endif // !UTIL_H
