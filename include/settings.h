#ifndef SETTING_H
#define SETTING_H

#include <lambda_types.h>
// typedef struct {
//   const char *symbol;
//   void (*arrange)(Monitor *);
// } Layout;

typedef struct {
  const char *name;
  float mfact;
  int nmaster;
  float scale;
  // const Layout *lt;
  enum wl_output_transform rr;
} MonitorRule;

typedef struct {
  const char *id;
  const char *title;
  unsigned int tags;
  int isfloating;
  int monitor;
} Rule;

static const Rule default_rules[] = {
	/* app_id     title       tags mask     isfloating   monitor */
	/* examples:
	{ "Gimp",     NULL,       0,            1,           -1 },
	*/
	{ "firefox",  NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
// static const Layout default_layouts[] = {
// 	/* symbol     arrange function */
// 	{ "><>",      NULL },    /* no layout function means floating behavior */
// };

/* monitors */
static const MonitorRule default_monrules[] = {
	/* name       mfact nmaster scale rotate/reflect */
	/* example of a HiDPI laptop monitor:
	{ "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL },
	*/
	/* defaults */
	{ NULL,       0.55, 1,      1,    WL_OUTPUT_TRANSFORM_NORMAL },
};
#endif
